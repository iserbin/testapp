package com.iserbin.testapp.utils;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class Constants {

    public static final String AUTH_TOKEN = "auth_token";
    public static final String PREFERENCES_NAME = "prefs";
    public static final String TESTAPI_SERVER_ADDRESS = "https://dl.dropboxusercontent.com/u/61732947/temp";
    public static final String POSITION = "position";
}
