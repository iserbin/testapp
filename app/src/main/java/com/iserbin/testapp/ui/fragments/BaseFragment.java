package com.iserbin.testapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iserbin.testapp.utils.Constants;

import iserbin.com.testapp.R;

/**
 * Created by Igorrr on 16.12.2014.
 */
public class BaseFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.base_fragment, container, false);
        int position = getArguments().getInt(Constants.POSITION);
        TextView text = (TextView) view.findViewById(R.id.tvPosition);
        text.setText(position+"");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
