package com.iserbin.testapp.ui;

/**
 * Created by Igorrr on 15.12.2014.
 */
public interface OnTaskCompletedCallback {
    void onTaskCompleted(String result);
}
