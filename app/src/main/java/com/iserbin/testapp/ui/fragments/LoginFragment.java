package com.iserbin.testapp.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.andreabaccega.widget.FormEditText;
import com.google.gson.Gson;
import com.iserbin.testapp.models.User;
import com.iserbin.testapp.network.FakeAuthAsyncTask;
import com.iserbin.testapp.ui.OnTaskCompletedCallback;
import com.iserbin.testapp.utils.Constants;

import iserbin.com.testapp.R;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class LoginFragment extends Fragment implements OnTaskCompletedCallback {

    private static final String TAG = "LoginFragment: ";
    private Button btnEnter;
    private FormEditText etLogin;
    private FormEditText etPassword;
    private LoginFragmentCallbacks mCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.fragment_login, container, false);
        setupUI(view);
        initViews();
        return view;
    }

    private void setupUI(View view) {
        etLogin = (FormEditText) view.findViewById(R.id.etLogin);
        etPassword = (FormEditText) view.findViewById(R.id.etPassword);
        btnEnter = (Button) view.findViewById(R.id.btnEnter);
    }

    private void initViews() {
        btnEnter.setOnClickListener(enterOnClickListener);
    }

    View.OnClickListener enterOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FormEditText[] allFields    = { etLogin, etPassword};
            boolean allValid = true;
            for (FormEditText field: allFields) {
                allValid = field.testValidity() && allValid;
            }

            if (allValid) {
                String prefix = "onClick: ";
                String login = etLogin.getText().toString().trim();
                String password = etPassword.getText().toString();
                Log.d(TAG, prefix + "login[" + login + "] password[" + password + "]");
                authenticateOnServerSide(login, password);
            }
        }
    };

    private void authenticateOnServerSide(String login, String password) {
        FakeAuthAsyncTask task = new FakeAuthAsyncTask(this);
        String url = "https://dl.dropboxusercontent.com/u/61732947/temp/auth.json";
        task.execute(url, login, password);
    }

    @Override
    public void onTaskCompleted(String result) {
        String prefix = "onTaskCompleted: ";
        Log.d(TAG, prefix+"result["+result+"]");

        if(result!=null) {
            Gson gson = new Gson();
            User user = gson.fromJson(result, User.class);
            Log.d(TAG, prefix+"user["+user+"]");
            saveUserInPrefsOrDB(user);
            if(mCallbacks!=null)
                mCallbacks.onUserDataReceived(user);
        }
        else
            Log.e(TAG, prefix+"received null response from server");
    }

    private void saveUserInPrefsOrDB(User user) {
        // saving user data
        // in our case only access token be saved cause it checked at app start
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(Constants.AUTH_TOKEN, user.getAccessToken()).apply();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (LoginFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public interface LoginFragmentCallbacks {
        void onUserDataReceived(User user);
    }
}
