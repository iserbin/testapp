package com.iserbin.testapp.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.iserbin.testapp.adapters.ListViewItemsAdapter;
import com.iserbin.testapp.models.ListViewItem;
import com.iserbin.testapp.network.GetListViewItemsAsynctask;
import com.iserbin.testapp.ui.OnListItemsFetchedCallback;

import java.util.List;

import iserbin.com.testapp.R;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class ListViewFragment extends Fragment implements OnListItemsFetchedCallback {

    private static final String TAG = "FirstFragment: ";
    private Button btnGetItems;
    private ListView lvItems;
    private ListViewItemsAdapter listViewItemsAdapter;
    private FrameLayout flButton;
    private FrameLayout flListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String prefix = "onCreateView: ";
        View view = inflater.inflate(R.layout.fragment_list_view, container, false);

        setupUI(view);
        initViews();

        return view;
    }

    private void setupUI(View view) {
        btnGetItems = (Button) view.findViewById(R.id.btnGetListItems);
        lvItems = (ListView) view.findViewById(R.id.lvItems);
        flButton = (FrameLayout) view.findViewById(R.id.flButton);
        flListView = (FrameLayout) view.findViewById(R.id.flListView);
    }

    private void initViews() {
        btnGetItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getListViewItems();
            }
        });
    }

    private void getListViewItems() {
        GetListViewItemsAsynctask task = new GetListViewItemsAsynctask(this);
        task.execute();
    }

    @Override
    public void onListItemsFetched(List<ListViewItem> itemsList) {
        String prefix = "onListItemsFetched: ";
//        Log.d(TAG, prefix+"fired");

        ListViewItem[] itemsArray = new ListViewItem[itemsList.size()];
        itemsList.toArray(itemsArray);
//        Log.d(TAG, prefix + "out["+ Arrays.toString(itemsArray)+"]");
        listViewItemsAdapter = new ListViewItemsAdapter(getActivity(), itemsArray);
        lvItems.setAdapter(listViewItemsAdapter);
        flButton.setVisibility(View.INVISIBLE);
        flListView.setVisibility(View.VISIBLE);
    }
}
