package com.iserbin.testapp.ui;

/**
 * Created by Igorrr on 15.12.2014.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
