package com.iserbin.testapp.ui;

import com.iserbin.testapp.models.ListViewItem;

import java.util.List;

/**
 * Created by Igorrr on 15.12.2014.
 */
public interface OnListItemsFetchedCallback {
    void onListItemsFetched(List<ListViewItem> items);
}
