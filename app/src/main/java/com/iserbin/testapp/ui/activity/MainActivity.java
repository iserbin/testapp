package com.iserbin.testapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.iserbin.testapp.ui.NavigationDrawerCallbacks;
import com.iserbin.testapp.ui.fragments.BaseFragment;
import com.iserbin.testapp.ui.fragments.ListViewFragment;
import com.iserbin.testapp.ui.fragments.NavigationDrawerFragment;
import com.iserbin.testapp.utils.Constants;

import iserbin.com.testapp.R;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class MainActivity extends ActionBarActivity implements NavigationDrawerCallbacks{

    private static final String TAG = "MainActivity: ";
    private FragmentManager manager;
    private DrawerLayout drawerLayout;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ActionBarDrawerToggle toggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //opening transition animations
//        overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
//        startService(new Intent(this, GiftAwayControllerService.class));

        final String prefix = "onCreate: ";
        manager = getSupportFragmentManager();
        setContentView(R.layout.activity_main);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout)findViewById(R.id.drawer), mToolbar);
        toggle = mNavigationDrawerFragment.getActionBarDrawerToggle();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return toggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        switch(position){
            case 0:
                startListViewFragment();
                break;
            case 6:
                logout();
                break;
            default:
                startBaseFragment(position);
        }
    }

    private void startBaseFragment(int position) {
        Bundle args = new Bundle();
        args.putInt(Constants.POSITION, position);
        Fragment detail = new BaseFragment();
        detail.setArguments(args);
        manager.beginTransaction().replace(R.id.containerMain, detail).commit();
    }

    private void logout() {
        // in our case we clear token
        SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.clear().apply();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void startListViewFragment() {
        ListViewFragment listViewFragment = new ListViewFragment();
        manager.beginTransaction().replace(R.id.containerMain, listViewFragment).commit();
    }
}
