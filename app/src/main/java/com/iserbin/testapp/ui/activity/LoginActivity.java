package com.iserbin.testapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

import com.iserbin.testapp.models.User;
import com.iserbin.testapp.ui.fragments.LoginFragment;

import iserbin.com.testapp.R;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class LoginActivity extends ActionBarActivity implements LoginFragment.LoginFragmentCallbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null)
            actionBar.hide();

        setContentView (R.layout.activity_login);
        if (savedInstanceState == null) {
            FragmentManager supportFragmentManager = getSupportFragmentManager();
            LoginFragment loginFragment = new LoginFragment();
            supportFragmentManager.beginTransaction()
                    .add(R.id.containerLogin, loginFragment)
                    .commit();
        }
    }

    @Override
    public void onUserDataReceived(User user) {
        if(user!=null){
            Intent intent = new Intent(this, MainActivity.class);
            Bundle bundle = new Bundle();
            // here we can serialize useful data ant put it to bundle or we can take later our DB stored values
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }
}
