package com.iserbin.testapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.iserbin.testapp.utils.Constants;

import iserbin.com.testapp.R;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class SplashActivity extends ActionBarActivity {

    private Context context = this;
    private TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        int appVersion = getAppVersion(context);

        setupUI();
        fillData(appVersion);

        int secondsDelayed = 5;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                String token = getSharedPreferences(Constants.PREFERENCES_NAME, MODE_PRIVATE).getString(Constants.AUTH_TOKEN, null);
                boolean logged = (token!=null && !token.isEmpty() && isTokenValid(token));
                if(logged){
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
                else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
                finish();

            }
        }, secondsDelayed * 1000);
    }

    private boolean isTokenValid(String token) {
        // in this place we can check lifetime of token or/and other issues
        return true;
    }

    private void fillData(int appVersion) {
        String text = (tvVersion.getText().toString()).replace("###", appVersion + "");
        tvVersion.setText(text);
    }

    private void setupUI() {
        tvVersion = (TextView) findViewById(R.id.tvVersion);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null)
            actionBar.hide();
    }

    private int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
