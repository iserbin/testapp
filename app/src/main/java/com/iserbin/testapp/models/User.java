package com.iserbin.testapp.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class User {
    private String accessToken;
    private String balance;
    private String city;
    private String countryCode;
    private String dateCreated;
    private String email;
    private Number emailIsVerified;
    private String name;
    private String region;
    private String regionID;
    private String status;
    private Number userID;

    public String getAccessToken(){
        return this.accessToken;
    }
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken;
    }
    public String getBalance(){
        return this.balance;
    }
    public void setBalance(String balance){
        this.balance = balance;
    }
    public String getCity(){
        return this.city;
    }
    public void setCity(String city){
        this.city = city;
    }
    public String getCountryCode(){
        return this.countryCode;
    }
    public void setCountryCode(String countryCode){
        this.countryCode = countryCode;
    }
    public String getDateCreated(){
        return this.dateCreated;
    }
    public void setDateCreated(String dateCreated){
        this.dateCreated = dateCreated;
    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public Number getEmailIsVerified(){
        return this.emailIsVerified;
    }
    public void setEmailIsVerified(Number emailIsVerified){
        this.emailIsVerified = emailIsVerified;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getRegion(){
        return this.region;
    }
    public void setRegion(String region){
        this.region = region;
    }
    public String getRegionID(){
        return this.regionID;
    }
    public void setRegionID(String regionID){
        this.regionID = regionID;
    }
    public String getStatus(){
        return this.status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public Number getUserID(){
        return this.userID;
    }
    public void setUserID(Number userID){
        this.userID = userID;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
