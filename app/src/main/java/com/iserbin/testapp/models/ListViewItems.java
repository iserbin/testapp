package com.iserbin.testapp.models;

import java.util.List;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class ListViewItems {
    private List<ListViewItem> items;

    public List<ListViewItem> getItems(){
        return this.items;
    }
    public void setItems(List<ListViewItem> items){
        this.items = items;
    }
}
