package com.iserbin.testapp.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class ListViewItem {

    public String text;
    public String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
