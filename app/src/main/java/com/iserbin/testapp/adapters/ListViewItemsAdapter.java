package com.iserbin.testapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserbin.testapp.models.ListViewItem;
import com.iserbin.testapp.network.FetchAndDisplaySVGTask;

import iserbin.com.testapp.R;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class ListViewItemsAdapter extends BaseAdapter{

    private static final String TAG = "ItemsAdapter: ";
    private ListViewItem[] items;
    Context mContext;

    public ListViewItemsAdapter(Context context, ListViewItem[] items) {
        this.mContext = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return (items!=null)?items.length:0;
    }

    @Override
    public ListViewItem getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder();
            holder.textLine = (TextView) convertView.findViewById(R.id.tvListItemText);
            holder.imageView = (ImageView) convertView.findViewById(R.id.ivListItemImage);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        ListViewItem item = items[position];
        if(item != null) {
            String prefix = "item data: ";
            Log.d(TAG, prefix+"text["+item.getText()+"] image["+item.getImage()+"]");
            holder.textLine.setText(item.text);

            String itemImageUrl = item.getImage();
            if(itemImageUrl !=null && !itemImageUrl.isEmpty()){
                String[] strings = itemImageUrl.split("\\.");
                if("svg".equals(strings[strings.length-1]))
                    showSVG(itemImageUrl, holder.imageView);
            }
            else {
                // we must show stub here or some else reaction
            }
        }
        return convertView;
    }

    private void showSVG(String svgUrl, ImageView imageView) {
        imageView.setTag(svgUrl);
        FetchAndDisplaySVGTask fetchAndDisplaySVGTask = new FetchAndDisplaySVGTask(mContext);
        fetchAndDisplaySVGTask.execute(imageView);
    }

    private class ViewHolder {
        public TextView textLine;
        public ImageView imageView;
    }
}
