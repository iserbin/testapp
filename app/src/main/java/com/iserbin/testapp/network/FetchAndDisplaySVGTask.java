package com.iserbin.testapp.network;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.caverock.androidsvg.SVG;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class FetchAndDisplaySVGTask extends AsyncTask<ImageView, Void, Drawable> {

    private static final String TAG = "GetSVGInputStreamTask: ";
    private final Context context;
    private ImageView imageView;

    public FetchAndDisplaySVGTask(Context context) {
        this.context = context;
    }

    @Override
    protected Drawable doInBackground(ImageView... imageViews) {
        this.imageView = imageViews[0];
        String url = (String) imageView.getTag();
        Drawable drawable = null;

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(request);
            InputStream inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null) {
                SVG svg = SVG.getFromInputStream(inputStream);
                drawable = new PictureDrawable(svg.renderToPicture());
            }
        } catch(Exception e) {
            String prefix = "doInBackground: ";
            Log.e(TAG, prefix + "got error[" + e.getMessage() + "]", e);
        }
        return drawable;
    }

    @Override
    protected void onPostExecute(Drawable drawable) {
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imageView.setImageDrawable(drawable);
    }
}
