package com.iserbin.testapp.network;

import android.os.AsyncTask;
import android.util.Log;

import com.iserbin.testapp.ui.OnTaskCompletedCallback;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class FakeAuthAsyncTask extends AsyncTask<String, Void, String> {

    private final OnTaskCompletedCallback listener;
    private String TAG = "FakeAuthAsyncTask: ";

    public FakeAuthAsyncTask(OnTaskCompletedCallback listener) {
        this.listener=listener;
    }

    @Override
    protected String doInBackground(String... params) {

        String prefix = "doInBackground: ";
        String url = params[0];
        String result = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost request = new HttpPost(url);

            List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("login", params[1]));
            postParameters.add(new BasicNameValuePair("password", params[2]));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
            request.setEntity(formEntity);

            HttpResponse httpResponse = httpClient.execute(request);
            InputStream inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
        } catch(Exception e) {
            Log.e(TAG, prefix+"got error["+e.getMessage()+"]", e);
        }
        return result;
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line;
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    protected void onPostExecute(String result) {
        if (listener == null)
            return;

        listener.onTaskCompleted(result);
    }
}
