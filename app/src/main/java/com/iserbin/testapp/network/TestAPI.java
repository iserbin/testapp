package com.iserbin.testapp.network;

import com.iserbin.testapp.models.ListViewItems;

import retrofit.http.GET;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class TestAPI {

    public static interface GetListViewItems {
        @GET("/getdata.json")
        ListViewItems getList();
    }
}
