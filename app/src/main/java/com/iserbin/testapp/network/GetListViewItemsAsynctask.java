package com.iserbin.testapp.network;

import android.os.AsyncTask;
import android.util.Log;

import com.iserbin.testapp.models.ListViewItem;
import com.iserbin.testapp.ui.OnListItemsFetchedCallback;
import com.iserbin.testapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Igorrr on 15.12.2014.
 */
public class GetListViewItemsAsynctask extends AsyncTask<Void, Void, List<ListViewItem>>{
    private static final String TAG = "GetListAsynctask: ";
    private final OnListItemsFetchedCallback listener;

    public GetListViewItemsAsynctask(OnListItemsFetchedCallback listener) {
        this.listener = listener;
    }

    @Override
    protected List<ListViewItem> doInBackground(Void... params) {
        String prefix = "doInBackground: ";
        Log.d(TAG, prefix + "fired");
        List<ListViewItem> result = new ArrayList<>();

        try {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.TESTAPI_SERVER_ADDRESS)
//                        .setClient(new OkClient(new OkHttpClient()))
                    .build();
            TestAPI.GetListViewItems rest_api = restAdapter.create(TestAPI.GetListViewItems.class);

            result = rest_api.getList().getItems();
        }
        catch (RetrofitError e) {
            Log.e(TAG, prefix+"error", e);
            Response response = e.getResponse();
            if (response!=null) {
                if (response.getBody() instanceof TypedByteArray) {
                    TypedByteArray array = (TypedByteArray) response.getBody();
                    Log.e(TAG, prefix + "Task error[" + e.getMessage() + "], error json: " + new String(array.getBytes()));
                } else {
                    Log.e(TAG, prefix + "Task error[" + e.getMessage() + "]");
                }
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(List<ListViewItem> listViewItems) {
        if(listener!=null)
            listener.onListItemsFetched(listViewItems);
    }
}
